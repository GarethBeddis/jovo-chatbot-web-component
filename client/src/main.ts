import JovoWebClientVue, { JovoWebClientVueConfig } from 'jovo-client-web-vue';
import Vue from 'vue';
import App from './App.vue';
import wrap from '@vue/web-component-wrapper';

Vue.config.productionTip = false;

Vue.use<JovoWebClientVueConfig>(JovoWebClientVue, { /* cut */ });

new Vue({
  render: (h) => h(App),
}).$mount('#app');

const wrappedElement = wrap(Vue, App);

window.customElements.define('my-custom-element', wrappedElement);
